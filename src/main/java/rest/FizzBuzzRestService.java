package rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import services.FizzBuzzService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Service
@Path("/fizzbuzzit")
public class FizzBuzzRestService extends BaseRestService {
    @Autowired
    FizzBuzzService fizzBuzzService;

    public FizzBuzzRestService(){
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response fizzBuzzIt(@QueryParam("input") String input){
        try {
            return buildOkResponse(fizzBuzzService.fizzBuzzString(input));
        } catch (IllegalArgumentException e){
            return buildErrorResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }
}
