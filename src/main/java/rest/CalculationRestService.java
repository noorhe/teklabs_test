package rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import services.calculator.CalculatorService;
import services.calculator.exceptions.InvalidStatementException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Service
@Path("calculate")
public class CalculationRestService extends BaseRestService {
    @Autowired
    CalculatorService calculatorService;
    public CalculationRestService(){
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response calculate(@QueryParam("expression")String expression) {
        try {
            return buildOkResponse(calculatorService.evaluate(expression));
        } catch (InvalidStatementException e) {
            return buildErrorResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }
}