package rest;

import javax.ws.rs.core.Response;

public class BaseRestService {
    protected Response buildOkResponse(String data){
        return Response.status(Response.Status.OK).entity(data).build();
    }

    protected Response buildErrorResponse(Response.Status status, String message){
        return Response.status(status).entity(message).build();
    }
}
