package config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import services.calculator.CalculatorService;
import services.calculator.CalculatorServiceImpl;

@Configuration
@ComponentScan(basePackages={"services"})
public class SpringConfig {
    @Bean
    public CalculatorService calculatorService(){
        return new CalculatorServiceImpl();
    }
}
