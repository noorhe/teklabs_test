package services;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class FizzBuzzService {
    private static final Logger LOGGER = Logger.getLogger(FizzBuzzService.class);

    private final String TOKEN_DELIMITER = " ";
    private final String ONLY_NUMBERS_AND_SPACES_REGEX = "([0-9]| )+";
    private final String DIVISIBLE_BY_THREE_WORD = "fizz";
    private final String DIVISIBLE_BY_FIVE_WORD = "buzz";
    private final String INTEGER_STRING_REGEX = "[0-9]+";

    public String fizzBuzzString(String input){
        String result;
        if(isInputValid(input)){
            result = Arrays.stream(input.split(TOKEN_DELIMITER))
                    .map(str -> isStrMatchesRegex(str, INTEGER_STRING_REGEX) ? fizzBuzzFromIntString(str) : str)
                    .collect(Collectors.joining(TOKEN_DELIMITER));
        } else {
            LOGGER.error("Error while fizz-buzzing str = " + input);
            throw new IllegalArgumentException("String for fizz buzz must contain only numbers and spaces.");
        }
        return result;
    }

    private boolean isInputValid(String input){
        boolean result = false;
        if(input != null){
            if(input.length() > 0){
                result = isStrMatchesRegex(input, ONLY_NUMBERS_AND_SPACES_REGEX);
            } else {
                result = true;
            }
        }
        return result;
    }

    private boolean isStrMatchesRegex(String input, String regex) {
        return Pattern.compile(regex).matcher(input).matches();
    }

    private String fizzBuzzFromIntString(String intStr){
        String result = intStr;
        final int integer = Integer.valueOf(intStr);
        final boolean isDivisibleByThree = integer % 3 == 0;
        final boolean isDivisibleByFive = integer % 5 == 0;
        if(isDivisibleByThree && isDivisibleByFive){
            result = DIVISIBLE_BY_THREE_WORD + " " + DIVISIBLE_BY_FIVE_WORD;
        } else if (isDivisibleByThree){
            result = DIVISIBLE_BY_THREE_WORD;
        } else if (isDivisibleByFive){
            result = DIVISIBLE_BY_FIVE_WORD;
        }
        return result;
    }
}
