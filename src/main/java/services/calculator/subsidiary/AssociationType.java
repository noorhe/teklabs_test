package services.calculator.subsidiary;
/**
 * The enumeration to define the type of the operator association (left or right).
 * @author Pavel Volosov
 *
 */
public enum AssociationType {
	LEFT,
	RIGHT
}
