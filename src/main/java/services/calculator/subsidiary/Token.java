package services.calculator.subsidiary;
/**
 * Base class for all tokens. Can store only a string value of the token. The particular type and attributes of token
 * are specified in subclasses.
 * @author Pavel Volosov
 *
 */
public class Token {
	protected String token;
	
	public Token(String token) {
		super();
		this.token = token;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
