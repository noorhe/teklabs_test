package services.calculator.subsidiary;
/**
 * Class for store number token with particular value.
 * @author Pavel Volosov
 *
 */
public class Number extends Token {
	public Number(String token) {
		super(token);
	}

}
