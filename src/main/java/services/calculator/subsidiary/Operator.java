package services.calculator.subsidiary;
/**
 * A class for store an operator token with particular attributes (association and priority (2 for + -, 3 for * /).
 * @author Pavel Volosov
 *
 */
public class Operator extends Token {
	private int priority;
	private AssociationType associationType;
	
	public Operator(String token, int priority, AssociationType assotiationType) {
		super(token);
		this.priority = priority;
		this.associationType = assotiationType;
	}
	
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}

	public AssociationType getAssociationType() {
		return associationType;
	}
}
