package services.calculator.subsidiary;
/**
 * A class for store a bracket token with particular attributes (bracket type).
 * @author Pavel Volosov
 *
 */
public class Bracket extends Token {
	BracketType bracketType;
	
	public Bracket(String token, BracketType bracketType) {
		super(token);
		this.bracketType = bracketType;
	}
	public BracketType getBracketType() {
		return bracketType;
	}
}
