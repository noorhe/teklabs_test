package services.calculator.subsidiary;
/**
 * The enumeration to define the type of the bracket (opening or closing).
 * @author Pavel Volosov
 *
 */
public enum BracketType {
	OPENING,
	CLOSING
}
