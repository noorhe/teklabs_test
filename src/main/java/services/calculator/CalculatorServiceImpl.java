package services.calculator;

import org.apache.log4j.Logger;
import services.calculator.exceptions.DivisionByZeroException;
import services.calculator.exceptions.InvalidBracketsStateException;
import services.calculator.exceptions.NotAllowedCharacterException;
import services.calculator.subsidiary.*;
import services.calculator.subsidiary.Number;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Stack;
/**
 * @author Pavel Volosov
 * The class is a simple calculator. It can take an equation with 0..9 digits and / * - + characters and return a result of it.
 */
public class CalculatorServiceImpl implements CalculatorService {
	private final static Logger LOGGER = Logger.getLogger(CalculatorServiceImpl.class);
    public static final String MISMATCHING_PARENTHESIS_ERROR_TEXT = "Looks like you have mismatching parenthesis. Check your expression.";
	public static final int RESULT_DEFAULT_SCALE = 0;

	@Override
	public String evaluate(String statement) {
		String result;
        try {
            ArrayList<Token> tokenList = tokenizeString(statement);
            Stack<Token> RPN = parseToPostfix(tokenList);
            Stack<Token> evaluationResult = calculate(RPN);
            result = new BigDecimal(Double.parseDouble(evaluationResult.pop().getToken())).setScale(RESULT_DEFAULT_SCALE, RoundingMode.UP).toPlainString();
        } catch (InvalidBracketsStateException e){
            LOGGER.error("Mismatching parenthesis in statement = " + statement, e);
            throw e;
        } catch (Throwable t){
			LOGGER.error("Exception for statement = " + statement, t);
			throw t;
		}
		return result;
	}
	/**
	 * The method transforms an input string into the set of mathematical tokens
	 * @param str equation string. There should be only digits and ( ) - = * / symbols with no spaces between them.
	 * @return returns a stack with all digits and symbols transformed to tokens
	 */
	private ArrayList<Token> tokenizeString(String str) {
		ArrayList<Token> result = new ArrayList<>();
		String currentTokenStr = "";
		Token currentToken = null;
		//Every symbol in the input string transforms becames a part of number token or of bracket or operator token
		for(int i=0; i < str.length(); i++){
			char currentChar = str.charAt(i);
			switch (currentChar) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '.':
					currentTokenStr += currentChar;
				break;
				//Separate token type with opening or closing attribute for brackets creating
				case '(':
					currentToken = new Bracket("" + currentChar, BracketType.OPENING);
				break;
				case ')':
					currentToken = new Bracket("" + currentChar, BracketType.CLOSING);
				break;
				//Separate token type with association (only "left" option in this task) and  priority
				//(only two options: 2 for + and - and 3 for * and / in this task, but could be more if additional operators support will need)
				case '+':
				case '-':
					currentToken = new Operator("" + currentChar, 2, AssociationType.LEFT);
				break;
				case '*':
				case '/':
					currentToken = new Operator("" + currentChar, 3, AssociationType.LEFT);
				break;
				default:
					//If there is a symbol, that could not be processed to token
					LOGGER.error("Wrong symbol '" + currentChar + "' in expression = " + str);
					throw new NotAllowedCharacterException("Only 0-9, arithmetical symbols and parenthesis allowed. " +
							"'" + currentChar + "' char not allowed.");
			}
			if(currentToken != null){
				if(!currentTokenStr.equals(""))
					result.add(new Number(currentTokenStr));
				currentTokenStr = "";
				result.add(currentToken);
				currentToken = null;
			}
		}
		if(!currentTokenStr.equals(""))
			result.add(new Number(currentTokenStr));
		return result;
	}
	/**
	 * The method transforms an expression in infix form (commonly used in maths) to postfix form, that can be easy calculated because there
	 * are no brackets in it and priorities of operations are defined only b� particular operation position in the statement.
	 * This method is a simplified realization of the Shutting-yard algorithm described <a href="http://en.wikipedia.org/wiki/Shunting-yard_algorithm">here.</a> 
	 * @param input a stack with valid tokens
	 * @return a stack with valid tokens following in postfix order.
	 * @throws InvalidBracketsStateException throws if there is an opening bracket with no closing and vice verse. 
	 */
	private Stack<Token> parseToPostfix(ArrayList<Token> input) {
		Stack<Token> output = new Stack<>();
		Stack<Token> tmp = new Stack<>();
		
		//Look http://en.wikipedia.org/wiki/Shunting-yard_algorithm for more details 
		for (Token currentToken : input) {
			if (currentToken instanceof Number)
				output.push(currentToken);
			else if (currentToken instanceof Bracket) {
				Bracket bracket = (Bracket) currentToken;
				if (bracket.getBracketType() == BracketType.OPENING)
					tmp.push(currentToken);
				else
					try {
						for (Token upperToken = tmp.pop(); !upperToken.getToken().equals("("); upperToken = tmp.pop())
							output.push(upperToken);
					} catch (EmptyStackException e) {
						throw new InvalidBracketsStateException(MISMATCHING_PARENTHESIS_ERROR_TEXT);
					}
			} else if (currentToken instanceof Operator) {
				Operator operator1 = (Operator) currentToken;
				if (!tmp.empty()) {
					for (Token upperToken = tmp.peek(); upperToken instanceof Operator; upperToken = tmp.peek()) {
						Operator operator2 = (Operator) upperToken;
						if (((operator1.getAssociationType() == AssociationType.LEFT && operator1.getPriority() <= operator2.getPriority()) ||
								(operator1.getAssociationType() == AssociationType.RIGHT && operator1.getPriority() < operator2.getPriority()))
								&& !tmp.empty())
							output.push(tmp.pop());
						if (!tmp.empty())
							break;
						else
							break;

					}
				}
				tmp.push(currentToken);
			}
		}
		int size = tmp.size();
		for(int i = 0; i < size; i++){
			Token currentToken = tmp.pop();
			if (currentToken instanceof Bracket){
				throw new InvalidBracketsStateException(MISMATCHING_PARENTHESIS_ERROR_TEXT);
			}
			else
				output.push(currentToken);
			
		}
		return output;
	}
	/**
	 * The method calculates a value of expression written in a reverse polish notation form.
	 * @param inputStack stack of tokens in RPN order.
	 * @return a stack with only one token member - number token with a result of expression.
	 */
	private Stack<Token> calculate(Stack<Token> inputStack){
		//Because we need to take a token from the bottom of the stack while evaluating an expression
		//it is convenient to form a queue from a stack to get a FIFO order of tokens 
		ArrayDeque<Token> inputQueue = new ArrayDeque<>(inputStack);
		Stack<Token> outputStack = new Stack<>();
		do{
			Token currentToken = inputQueue.removeFirst();
			if (currentToken instanceof Number)
				outputStack.push(currentToken);
			else
				if (currentToken instanceof Operator){
					Double rightOperand = Double.parseDouble((outputStack.pop()).getToken());
					Double leftOperand = Double.parseDouble((outputStack.pop()).getToken());
					Operator operator = (Operator)currentToken;
					switch (operator.getToken()) {
						case "+": {
							Double result = leftOperand + rightOperand;
							outputStack.push(new Number(result.toString()));
							break;
						}
						case "-": {
							Double result = leftOperand - rightOperand;
							outputStack.push(new Number(result.toString()));
							break;
						}
						case "*": {
							Double result = leftOperand * rightOperand;
							outputStack.push(new Number(result.toString()));
							break;
						}
						case "/": {
							if(rightOperand == 0f){
								String errorText = "Attempt to divide " + leftOperand + " by 0";
								LOGGER.error(errorText);
								throw new DivisionByZeroException(errorText);
							}
							Double result = leftOperand / rightOperand;
							outputStack.push(new Number(result.toString()));
							break;
						}
					}
				}
		}
		while(inputQueue.size() > 0);
		return outputStack;
	}
}
