package services.calculator.exceptions;
/**
 * 
 * @author Pavel Volosov
 * A special exception that throws when there is an opening bracket with no closing (and vice verse).
 */
public class InvalidBracketsStateException extends InvalidStatementException {
    public InvalidBracketsStateException(String message){
        super(message);
    }
}
