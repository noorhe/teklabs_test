package services.calculator.exceptions;
/**
 * 
 * @author Pavel Volosov
 * A special exception that throws when there is not allowed characters in the input expression.
 */
public class NotAllowedCharacterException extends InvalidStatementException {
    public NotAllowedCharacterException(String message){
        super(message);
    }
}
