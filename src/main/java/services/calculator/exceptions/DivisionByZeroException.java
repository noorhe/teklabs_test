package services.calculator.exceptions;

public class DivisionByZeroException extends InvalidStatementException {
    public DivisionByZeroException(String message) {
        super(message);
    }
}
