package main;

import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;
import java.io.File;

public class Main {
    public static void main(String[] args) throws Exception {
        Tomcat tomcat = initializeTomcat();
        initializeTomcatContext(tomcat);
        tomcat.start();
        tomcat.getServer().await();
    }

    private static void initializeTomcatContext(Tomcat tomcat) throws ServletException {
        final String STATIC_WEB_CONTENT_DIR = "src/main/resources/webapp/WebContent/";
        StandardContext tomcatContext = (StandardContext) tomcat.addWebapp("/", new File(STATIC_WEB_CONTENT_DIR).getAbsolutePath());
        tomcatContext.addWelcomeFile("index.html");
    }

    private static Tomcat initializeTomcat() {
        Tomcat tomcat = new Tomcat();
        String webPort = System.getenv("PORT");
        if(webPort == null || webPort.isEmpty()) {
            webPort = "8080";
        }
        tomcat.setPort(Integer.valueOf(webPort));
        return tomcat;
    }
}