package main;

import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public enum SpringContextHolder {
    HOLDER;
    AnnotationConfigWebApplicationContext context;
    void init(AnnotationConfigWebApplicationContext ctx){
        this.context = ctx;
    }

    public AnnotationConfigWebApplicationContext getContext() {
        return context;
    }
}
