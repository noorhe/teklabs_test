package main;

import config.SpringConfig;
import org.apache.log4j.BasicConfigurator;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class WebAppInitializer implements WebApplicationInitializer{
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        initializeSpringContext();
        configureServletContext(servletContext, SpringContextHolder.HOLDER.getContext());
        initializeLogger();
    }

    private void initializeSpringContext() {
        AnnotationConfigWebApplicationContext context = buildSpringContext();
        SpringContextHolder.HOLDER.init(context);
    }

    private AnnotationConfigWebApplicationContext buildSpringContext() {
        AnnotationConfigWebApplicationContext springContext = new AnnotationConfigWebApplicationContext();
        springContext.register(SpringConfig.class);
        return springContext;
    }

    private void configureServletContext(ServletContext servletContext, AnnotationConfigWebApplicationContext springContext) {
        servletContext.addListener(new ContextLoaderListener(springContext));
        ServletRegistration.Dynamic restServlet = servletContext.addServlet("restServlet", resourceConfig());
        restServlet.addMapping("/service/*");
    }

    private ServletContainer resourceConfig() {
        return new ServletContainer(new ResourceConfig(
                new ResourceLoader().getClasses()));
    }

    private void initializeLogger() {
        BasicConfigurator.configure();
    }
}
