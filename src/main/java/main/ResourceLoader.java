package main;

import rest.FizzBuzzRestService;
import rest.CalculationRestService;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ResourceLoader extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(CalculationRestService.class);
        classes.add(FizzBuzzRestService.class);
        return classes;
    }
}
