var rootUrl = null;
var BAD_REQUEST_STATUS = 400;

function init(){
    initInputButtonPare("fizz_buzz_input", "fizz_buzz_it_button", "service/fizzbuzzit", "input");
    initInputButtonPare("calculate_it_input", "calculate_it_button", "service/calculate", "expression");
}

function initInputButtonPare(inputId, buttonId, servicePath, paramaName){
    var button = $("#" + buttonId);
    button.unbind();
    button.click(function(){
        var value = encodeURIComponent($("#" + inputId).val());
        doServiceGetQuery(servicePath, paramaName, value)
            .done(function(data){
                $("#" + inputId).val(data);
            })
            .error(function(data){
                if(data.status === BAD_REQUEST_STATUS){
                    alert(data.responseText);
                }
            });
        return false;
    });
}

function doServiceGetQuery(servicePath, paramName, value){
    return $.get(servicePath + "?" + paramName + "=" + value);
}