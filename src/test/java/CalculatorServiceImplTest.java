import config.SpringConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.calculator.CalculatorService;
import services.calculator.exceptions.InvalidStatementException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class CalculatorServiceImplTest {
    @Autowired
    CalculatorService calculatorService;
    Map<String, String> validInputTestData;
    {
        validInputTestData = new HashMap<>();
        validInputTestData.put("1", "1");
        validInputTestData.put("2+2", "4");
        validInputTestData.put("2*2", "4");
        validInputTestData.put("2/2", "1");
        validInputTestData.put("2-2", "0");
        validInputTestData.put("2+(2*2)", "6");
    }

    List<String> invalidInputTestData;
    {
        invalidInputTestData = new ArrayList<>();
        invalidInputTestData.add("2/0");
        invalidInputTestData.add("asd");
        invalidInputTestData.add("2+(1");
        invalidInputTestData.add("2+2+23s");
    }

    @Test
    public void testFizzBuzzStringPositive() {
        for (String input : validInputTestData.keySet()) {
            Assert.assertEquals(validInputTestData.get(input), calculatorService.evaluate(input));
        }
    }

    @Test
    public void testFizzBuzzNegative(){
        for (String input : invalidInputTestData){
            try{
                calculatorService.evaluate(input);
            } catch (Throwable e){
                Assert.assertTrue(e instanceof InvalidStatementException);
            }
        }
    }
}
