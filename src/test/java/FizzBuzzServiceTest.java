import config.SpringConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.FizzBuzzService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class FizzBuzzServiceTest {
    @Autowired
    FizzBuzzService fizzBuzzService;
    Map<String, String> validInputTestData;
    {
        validInputTestData = new HashMap<>();
        validInputTestData.put("", "");
        validInputTestData.put("1", "1");
        validInputTestData.put("3", "fizz");
        validInputTestData.put("5", "buzz");
        validInputTestData.put("15", "fizz buzz");
        validInputTestData.put("3 5", "fizz buzz");
        validInputTestData.put("1 2 3 4 5 6 7 8 9 10", "1 2 fizz 4 buzz fizz 7 8 fizz buzz");
        validInputTestData.put("3     5", "fizz     buzz");
    }

    List<String> invalidInputTestData;
    {
        invalidInputTestData = new ArrayList<>();
        invalidInputTestData.add("sdajhdas");
        invalidInputTestData.add("23 ss ");
    }

    @Test
    public void testFizzBuzzStringPositive() {
        for (String input : validInputTestData.keySet()) {
            Assert.assertEquals(validInputTestData.get(input), fizzBuzzService.fizzBuzzString(input));
        }
    }

    public void testFizzBuzzNegative(){
        for (String input : invalidInputTestData){
            try{
                fizzBuzzService.fizzBuzzString(input);
            } catch (Throwable t){
                Assert.assertTrue(t instanceof IllegalArgumentException);
            }
        }
    }
}
